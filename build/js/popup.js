jQuery(document).ready(function($){
	//open popup
	$('.cd-popup-trigger').on('click', function(event){
		event.preventDefault();
		$('.cd-popup-one').addClass('is-visible');
        var id = $(this).parent().parent().parent().attr("id");
        $('.cd-popup-three').append("<input id='item_id' type='hidden' name='item_id' value='"+id+"'>");
	});
    $("#fio1").on("keyup",function(e){
        e.preventDefault();
        if($(this).next().hasClass("error")) $(this).next().remove();
    })
    $("#email1").on("keyup",function(e){
        e.preventDefault();
        if($(this).next().hasClass("error")) $(this).next().remove();
    })
    $("input[name=\"mobile\"]").inputmask({ 'mask': '+7 (999) 999-99-99', 'placeholder' : 'x' });
    $("input[name=\"mobile\"]").on("keyup",function(e){
        e.preventDefault();
        if($(this).next().hasClass("error")) $(this).next().remove();
    })
	$('#submit1').on('click', function(event){
		event.preventDefault();
        var fio = $("#fio1").val();
        var mobile = $("#mobile1").val();
        var email = $("#email1").val();
        if(fio == "" || fio == undefined) {
            $("<p class=\"error\">Укажите Ваше Имя.</p>").insertAfter("#fio1");
            return false;
        }
        if(email == "" || email == undefined) {
            $("<p class=\"error\">Укажите Ваш e-mail.</p>").insertAfter("#email1");
            return false;
        }
        if(mobile == "" || mobile == undefined) {
            $("<p class=\"error\">Укажите Ваш мобильный телефон.</p>").insertAfter("#mobile1");
            return false;
        }
        //alert($(this).parent().parent().attr("class"));
        var _this = $(this);
        $.post("ajax.php", { fio: fio, mobile: mobile,  email: email })
            .done(function( data ) {
                $(_this).parent().parent().html(data)
            });
	});
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$('.cd-popup').removeClass('is-visible');
	    }
    });
});