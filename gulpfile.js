"use strict";
var gulp = require("gulp"),
	clean = require("gulp-clean"),
	cache = require("gulp-cached"),
	util = require("gulp-util"),
	coffee = require("gulp-coffee"),
	concat = require("gulp-concat"),
	debug = require("gulp-debug"),
	gulpIf = require("gulp-if"),
	uglify = require("gulp-uglify"),
	rename = require("gulp-rename"),
	notify = require("gulp-notify"),
	source = require("vinyl-source-stream"),
	remember = require("gulp-remember"),
	combiner = require("stream-combiner2").obj,
	sourcemaps = require("gulp-sourcemaps"),
	browserify = require("browserify"),
	vinylBuffer = require("vinyl-buffer"),
	browserSync = require("browser-sync").create();


gulp.task("server", function(){
	browserSync.init({
		"server": "build" 
	});
	browserSync.watch("./build/**/*.*").on("change", browserSync.reload);
});


gulp.task("default", function(done){
	return gulp.series(
		"server"
	)(done);
});